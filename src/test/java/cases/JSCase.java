package cases;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.core.io.ClassPathResource;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.GooglePage;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class JSCase {

    private final String GOOGLE_PAGE_URL = "https://google.ru/";
    private String searchText;
    private RemoteWebDriver driver;
    private GooglePage googlePage;

    @BeforeClass
    public void setUp() throws Exception {
        ChromeDriverManager.getInstance().version("2.29").setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        initProperties();
    }

    @Test
    public void testJSExecutor() throws IOException {
        driver.get(GOOGLE_PAGE_URL);
        googlePage = new GooglePage(driver);
        int searchBoxSize = googlePage.getSearchBox().getSize().getWidth();
        System.out.println("SearchBox size = " + searchBoxSize + "px");
        Assert.assertFalse(isScrollBarPresent());
        googlePage.sendSearchText(searchText);
        lostFocusOnSearchInput();
        googlePage.submitSearchText();
        Assert.assertTrue(isScrollBarPresent());

    }

    private void lostFocusOnSearchInput() {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("return document.getElementById('lst-ib').blur()");
    }

    private boolean isScrollBarPresent() {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        return (boolean) jsExecutor.executeScript("return document.documentElement.scrollHeight>document.documentElement.clientHeight;");
    }


    private void initProperties() throws IOException {
        Properties config = new Properties();
        try {
            config.load(new ClassPathResource("config.properties").getInputStream());
        } catch (IOException ex) {
            ex.printStackTrace();
            fail("File 'config.properties' can't be open!");
        }
        searchText = config.getProperty("google.searchbox.text");

    }

    @AfterClass
    public void tearDown() throws Exception {
        driver.close();
        driver.quit();
    }
}
